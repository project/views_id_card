<?php

/**
 * @file
 * Definition of Drupal\badge\Plugin\views\style\Badge.
 */

namespace Drupal\badge\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render a list of years and months
 * in reverse chronological order linked to content.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "badge",
 *   title = @Translation("Badge"),
 *   help = @Translation("Render a list of years and months in reverse chronological order linked to content."),
 *   theme = "views_view_badge",
 *   display_types = { "normal" }
 * )
 *
 */
class BadgeStyle extends StylePluginBase {

  /**
   * Does the style plugin for itself support to add fields to its output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    
    $options = parent::defineOptions();
    
    $options['uses_fields'] = ['default' => TRUE];

    $options['id_field'] = ['default' => ''];
    $options['person_image_field'] = ['default' => ''];
    $options['first_name_field'] = ['default' => ''];
    $options['last_name_field'] = ['default' => ''];
    $options['category_field'] = ['default' => ''];
    $options['badge_image_field'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Get available fields from the view.
    $fields = $this->displayHandler->getFieldLabels(TRUE);

    // Populate select list with field options.
    $field_options = [];
    foreach ($fields as $field_id => $label) {
      $field_options[$field_id] = $label;
    }

    $form['id_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Unique ID Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['id_field'] ?? '',
    ];

    $form['person_image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Person Image Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['person_image_field'] ?? '',
    ];

    $form['first_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('First Name Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['first_name_field'] ?? '',
    ];

    $form['last_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Last Name Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['last_name_field'] ?? '',
    ];

    $form['category_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Category Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['category_field'] ?? '',
    ];

    $form['badge_image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Badge Image Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['badge_image_field'] ?? '',
    ];
  }
}