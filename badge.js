/**
 * @file
 * Global utilities.
 *
 */
(function (Drupal, $, once) {
  'use strict';

  var loaded = false;

  Drupal.behaviors.badge = {
    attach: function (context, settings) {

      if (context === document && !loaded) {
        loaded = true;
        const badgeButton = once.find('badge');
        $(badgeButton, context).click(function () {

          var badgesToPrint = $('#badges-to-print').find('.badge:not(.disabled)').map(function() {
            return $(this).prop('outerHTML');
          }).get().join('');

          var modulePath = drupalSettings.path.baseUrl + 'modules/custom/badge/';


          var printWindow = window.open('', '_blank');
          printWindow.document.write('<html><head><title>Badges</title>');

          printWindow.document.write('<link rel="stylesheet" type="text/css" href="' + modulePath + 'badge.css">');

          printWindow.document.write('</head><body class="badges">');
          printWindow.document.write(badgesToPrint);
          printWindow.document.write('</body></html>');
          printWindow.document.close();

          printWindow.onload = function () {
            printWindow.print();
          };

        });

        $(".slider-font-size", context).change(function () {
          var targetSelector = $(this).data('target');
          $(targetSelector).css('font-size', $(this).val() + 'rem');
        });

        $(".slider-image-width", context).change(function () {
          var targetSelector = $(this).data('target');
          $(targetSelector).css('width', $(this).val() + 'rem');
        });


        var v = 90;
        $(".button-image-rotation", context).click(function () {
          var targetSelector = $(this).data('target');
          $(targetSelector).css('transform', 'rotate(' + v + 'deg)');
          v += 90;
        });

        $(".dont-print-badge", context).change(function () {
          var targetSelector = $(this).data('target');
          $(targetSelector).toggleClass('disabled');
        });


        // Global Controls
        $(".global-first-name-size-slider").change(function () {
          var v = $(this).val();
          $('span.first-name').css('font-size', v + 'rem');
        });
        $(".global-last-name-size-slider").change(function () {
          var v = $(this).val();
          $('span.last-name').css('font-size', v + 'rem');
        });
        $(".global-student-image-slider").change(function () {
          var v = $(this).val();
          $('.img-container img').css('width', v + 'px');
        });


        var _DRAGGGING_STARTED = 0;
        var _LAST_MOUSE_POSITION = {
          x: null,
          y: null
        };
        var _DIV_OFFSET = $('.img-container').offset();
        var _CONTAINER_WIDTH = $(".img-container").outerWidth();
        var _CONTAINER_HEIGHT = $(".img-container").outerHeight();
        var _IMAGE_WIDTH;
        var _IMAGE_HEIGHT;
        var _IMAGE_LOADED = 0;

        if ($('.img-container img').get(0).complete) {
          ImageLoaded();
        } else {
          $('.img-container img').on('load', function () {
            ImageLoaded();
          });
        }

        // Image is loaded
        function ImageLoaded() {
          _IMAGE_WIDTH = $(".img-container img").width();
          _IMAGE_HEIGHT = $(".img-container img").height();
          _IMAGE_LOADED = 1;
        }

        $('.img-container').on('mousedown', function (event) {
          /* Image should be loaded before it can be dragged */
          if (_IMAGE_LOADED == 1) {
            _DRAGGGING_STARTED = 1;

            /* Save mouse position */
            _LAST_MOUSE_POSITION = {
              x: event.pageX - _DIV_OFFSET.left,
              y: event.pageY - _DIV_OFFSET.top
            };
          }
        });
        $('.img-container').on('mouseup', function () {
          _DRAGGGING_STARTED = 0;
        });

        $('.img-container').on('mousemove', function (event) {
          if (_DRAGGGING_STARTED == 1) {
            var current_mouse_position = {
              x: event.pageX - _DIV_OFFSET.left,
              y: event.pageY - _DIV_OFFSET.top
            };
            var change_x = current_mouse_position.x - _LAST_MOUSE_POSITION.x;
            var change_y = current_mouse_position.y - _LAST_MOUSE_POSITION.y;

            /* Save mouse position */
            _LAST_MOUSE_POSITION = current_mouse_position;

            var img_top = parseInt($(this).find("img").css('top'), 10);
            var img_left = parseInt($(this).find("img").css('left'), 10);

            var img_top_new = img_top + change_y;
            var img_left_new = img_left + change_x;

            $(this).find("img").css({
              top: img_top_new + 'px',
              left: img_left_new + 'px'
            });
          }
        });

        // Global
        $('.global-img-container').on('mousedown', function (event) {
          /* Image should be loaded before it can be dragged */
          if (_IMAGE_LOADED == 1) {
            _DRAGGGING_STARTED = 1;

            /* Save mouse position */
            _LAST_MOUSE_POSITION = {
              x: event.pageX - _DIV_OFFSET.left,
              y: event.pageY - _DIV_OFFSET.top
            };
          }
        });
        $('.global-img-container').on('mouseup', function () {
          _DRAGGGING_STARTED = 0;
        });
        $('.global-img-container').on('mousemove', function (event) {
          if (_DRAGGGING_STARTED == 1) {
            var current_mouse_position = {
              x: event.pageX - _DIV_OFFSET.left,
              y: event.pageY - _DIV_OFFSET.top
            };
            var change_x = current_mouse_position.x - _LAST_MOUSE_POSITION.x;
            var change_y = current_mouse_position.y - _LAST_MOUSE_POSITION.y;

            /* Save mouse position */
            _LAST_MOUSE_POSITION = current_mouse_position;

            var img_top = parseInt($(".img-container img").css('top'), 10);
            var img_left = parseInt($(".img-container img").css('left'), 10);

            var img_top_new = img_top + change_y;
            var img_left_new = img_left + change_x;

            $(".img-container img").css({
              top: img_top_new + 'px',
              left: img_left_new + 'px'
            });
          }
        });

      }

    }
  };

})(Drupal, jQuery, once);
