<?php

/**
 * @file
 * Theming for Badge views.
 */
function template_preprocess_views_view_badge(&$variables) {
    $variables['#attached']['library'][] = 'badge/badge';

    
    $view = $variables['view'];
    $rows = $variables['rows'];
    $style = $view->style_plugin;
    $options = $style->options;
    
    foreach ($view->result as $id => $row) {

        $id_field = $style->getfield($id, $options['id_field']);
        $person_image = $style->getField($id, $options['person_image_field']);
        $first_name = $style->getField($id, $options['first_name_field']);
        $last_name = $style->getField($id, $options['last_name_field']);
        $category = $style->getField($id, $options['category_field']);
        $badge_image = $style->getField($id, $options['badge_image_field']);

        $result = array();
        $result['id'] = $id_field;
        $result['person_image'] = $person_image;
        $result['first_name'] = $first_name;
        $result['last_name'] = $last_name;
        $result['category'] = $category;
        $result['badge_image'] = $badge_image;

        $rows[$id]->content = $result;

    }

}